# Datamodel Entities Module

This module provide a fieldable "datamodel" entity with an admin ui and exportable configuration to use to kick-start your entity development. It can be used out of the box to create new custom entities of type "datamodel" which you can base your custom entities. It also pairs nicely with [Entity Decorator D8 Module](https://www.drupal.org/sandbox/solomonrothman/2977603), so you can decorate yoru custom entities to add specific functionality without having to write sepeartate modules for each entity type. It also uses a generic name "datamodel", so depending on use case can be used directly for simple entity needs..

## Prior Work and Acknowledgements
This module is the Drupal 8 equivalent (Written from scratch for Drupal 8) of the [Drupal 7's Model Entities Module](https://www.drupal.org/project/model).

## Requirements

* Drupal 8

## Installation
Datamodel Entities Module can be installed via the
[standard Drupal installation process](http://drupal.org/node/895232).

## Configuration
 * Data model types can be added and edited here: /admin/structure/datamodel_type
 * Data models are listed:  /admin/structure/datamodel