<?php

/**
 * @file
 * Contains datamodel.page.inc.
 *
 * Page callback for Datamodel entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Datamodel templates.
 *
 * Default template: datamodel.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_datamodel(array &$variables) {
  // Fetch Datamodel Entity Object.
  $datamodel = $variables['elements']['#datamodel'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
