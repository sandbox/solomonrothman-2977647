<?php

namespace Drupal\datamodel;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for datamodel.
 */
class DatamodelTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
