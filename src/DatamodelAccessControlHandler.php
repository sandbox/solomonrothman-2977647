<?php

namespace Drupal\datamodel;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Datamodel entity.
 *
 * @see \Drupal\datamodel\Entity\Datamodel.
 */
class DatamodelAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\datamodel\Entity\DatamodelInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished datamodel entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published datamodel entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit datamodel entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete datamodel entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add datamodel entities');
  }

}
