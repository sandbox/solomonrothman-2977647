<?php

namespace Drupal\datamodel\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Datamodel type entity.
 *
 * @ConfigEntityType(
 *   id = "datamodel_type",
 *   label = @Translation("Datamodel type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\datamodel\DatamodelTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\datamodel\Form\DatamodelTypeForm",
 *       "edit" = "Drupal\datamodel\Form\DatamodelTypeForm",
 *       "delete" = "Drupal\datamodel\Form\DatamodelTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\datamodel\DatamodelTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "datamodel_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "datamodel",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/datamodel_type/{datamodel_type}",
 *     "add-form" = "/admin/structure/datamodel_type/add",
 *     "edit-form" = "/admin/structure/datamodel_type/{datamodel_type}/edit",
 *     "delete-form" = "/admin/structure/datamodel_type/{datamodel_type}/delete",
 *     "collection" = "/admin/structure/datamodel_type"
 *   }
 * )
 */
class DatamodelType extends ConfigEntityBundleBase implements DatamodelTypeInterface {

  /**
   * The Datamodel type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Datamodel type label.
   *
   * @var string
   */
  protected $label;

}
