<?php

namespace Drupal\datamodel\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Datamodel entities.
 *
 * @ingroup datamodel
 */
interface DatamodelInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Datamodel name.
   *
   * @return string
   *   Name of the Datamodel.
   */
  public function getName();

  /**
   * Sets the Datamodel name.
   *
   * @param string $name
   *   The Datamodel name.
   *
   * @return \Drupal\datamodel\Entity\DatamodelInterface
   *   The called Datamodel entity.
   */
  public function setName($name);

  /**
   * Gets the Datamodel creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Datamodel.
   */
  public function getCreatedTime();

  /**
   * Sets the Datamodel creation timestamp.
   *
   * @param int $timestamp
   *   The Datamodel creation timestamp.
   *
   * @return \Drupal\datamodel\Entity\DatamodelInterface
   *   The called Datamodel entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Datamodel published status indicator.
   *
   * Unpublished Datamodel are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Datamodel is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Datamodel.
   *
   * @param bool $published
   *   TRUE to set this Datamodel to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\datamodel\Entity\DatamodelInterface
   *   The called Datamodel entity.
   */
  public function setPublished($published);

}
