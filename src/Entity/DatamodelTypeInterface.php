<?php

namespace Drupal\datamodel\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Datamodel type entities.
 */
interface DatamodelTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
