<?php

namespace Drupal\datamodel\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class DatamodelTypeForm.
 */
class DatamodelTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $datamodel_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $datamodel_type->label(),
      '#description' => $this->t("Label for the Datamodel type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $datamodel_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\datamodel\Entity\DatamodelType::load',
      ],
      '#disabled' => !$datamodel_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $datamodel_type = $this->entity;
    $status = $datamodel_type->save();

    switch ($status) {
      case SAVED_NEW:
        drupal_set_message($this->t('Created the %label Datamodel type.', [
          '%label' => $datamodel_type->label(),
        ]));
        break;

      default:
        drupal_set_message($this->t('Saved the %label Datamodel type.', [
          '%label' => $datamodel_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($datamodel_type->toUrl('collection'));
  }

}
