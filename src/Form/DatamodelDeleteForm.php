<?php

namespace Drupal\datamodel\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Datamodel entities.
 *
 * @ingroup datamodel
 */
class DatamodelDeleteForm extends ContentEntityDeleteForm {


}
